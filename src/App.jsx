import { Routes, Route } from 'react-router-dom';
import Board from './components/Board/Board.jsx';
import Errorpage from './components/Errorpage.jsx';
import Lists from './components/List/Lists.jsx'
import Navbars from './components/Navbars.jsx';
import React, { useState, useEffect } from 'react';
import { getdata } from './components/getdata.js';


function App() {
  const [boards, setBoards] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    try {
      const data = await getdata("members", "boards");
      setBoards(data);
      setLoading(false);
    } catch (error) {
      setError("Cannot fetch boards");
      setLoading(false);
    }
  }

  return (
    <>
      <Navbars boards={boards} />
      <Routes>
        <Route exact path="/" element={<Board boards={boards} setBoards={setBoards} loading={loading} setLoading={setLoading} error={error} setError={setError} />} />
        <Route exact path='boards/' element={<Board boards={boards} setBoards={setBoards} loading={loading} setLoading={setLoading} error={error} setError={setError} />} />
        <Route exact path='boards/:id' element={<Lists />} />
        <Route path='*' element={<Errorpage />} />
        <Route path='*/*' element={<Errorpage />} />
      </Routes>

    </>
  )
}

export default App
