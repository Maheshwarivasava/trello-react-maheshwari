import { Add, Close } from "@mui/icons-material";
import { Box, Button, TextField } from "@mui/material";
import React, { useState } from "react";

const CreateForm = ({ addItem, itemName }) => {
    const [newItemName, setNewItemName] = useState('');
    const [showInput, setShowInput] = useState(false);

    const handleInputChange = (e) => {
        setNewItemName(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        addItem(newItemName);
        setNewItemName('');
        setShowInput(false);
    };

    const handleCancel = () => {
        setNewItemName('');
        setShowInput(false);
    };

    return (
        <Box>
            {!showInput && (
                <Button onClick={() => setShowInput(true)}>
                    Add a {`${itemName}`} 
                    <Add />
                </Button>
            )}
            {showInput && (
                <form onSubmit={handleSubmit}>
                    <TextField
                        label={`${itemName} Name`}
                        value={newItemName}
                        onChange={handleInputChange}
                        fullWidth
                        required
                    />
                    <Button type="submit">Create {itemName}</Button>
                    <Close onClick={handleCancel} sx={{ "&:hover": { cursor: "pointer" } }} />
                </form>
            )}
        </Box>
    )
}

export default CreateForm;
