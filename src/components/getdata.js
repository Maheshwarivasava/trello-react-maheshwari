import axios from "axios";

const TOKEN = import.meta.env.VITE_API_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

axios.defaults.params={
  key:KEY,
  token:TOKEN,
}
export async function getdata(from, item = "", id = "me") {
  // console.log(from, item, id);
  //   console.log(key);
  //   console.log(token);

  try {
    const response = await axios.get(
      `https://api.trello.com/1/${from}/${id}/${item}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    const data = response.data;
    // console.log(data);
    return data;
  } catch (error) {
    // console.error('Error fetching Data:', error);
    if (error.response.status === 400) {
      //   setError(`400`);
    }
  }
}

export async function addBoarddata(boardName) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/boards/?name=${boardName}`
    );
    // console.log(data);
    return response;
  } catch (error) {
    if (error.response.status === 400) {
      alert("This Board already exists");
    } else {
      alert("An Error Occurred!");
    }
  }
}

export async function addListdata(listName, id) {
  try {
    console.log(listName);
    let response = await axios.post(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${id}`
    );
    // console.log('Hello');
    // console.log(data);
    return response;
  } catch (error) {
    if (error.response.status === 400) {
      alert("This list already exists");
    } else {
      alert("An Error Occurred!");
    }
  }
}

export async function addCarddata(CardName, id) {
  try {
    // console.log(CardName);
    let response = await axios.post(
      `https://api.trello.com/1/cards?name=${CardName}&idList=${id}`
    );
    // console.log('Hello');
    return response;
  } catch (error) {
    if (error.response.status === 400) {
      alert("This Card already exists");
    } else {
      alert("An Error Occurred!");
    }
  }
}

export async function addCheckListdata(name, id) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/checklists?name=${name}&idCard=${id}`
    );
    return response;
  } catch (error) {
    if (error.response.status === 400) {
      alert("This Checklist already exists");
    } else {
      alert("An Error Occurred!");
    }
  }
}

export async function addCheckItemdata(name, id) {
  try {
    let response = await axios.post(
      `https://api.trello.com/1/checklists/${id}/checkItems?name=${name}`
    );
    return response;
  } catch (error) {
    if (error.response.status === 400) {
      alert("This Checkitem already exists");
    } else {
      alert("An Error Occurred!");
    }
  }
}

export async function checkItemStatedata(checked, checkitemID,cardID) {
  try {
    let response = await axios.put(
      `https://api.trello.com/1/cards/${cardID}/checkItem/${checkitemID}`,
      {
        state: checked,
      }
    );
    return response;
  } catch (error) {
    alert("An Error Occurred!");
  }
}


export async function deleteList(id) {

  // console.log('Hiii',id,TOKEN);
   await axios.put(
      `https://api.trello.com/1/lists/${id}/closed?value=true`
  );
 
}

export async function deleteCard(id) {
  // console.log(id);
  await axios.delete(
      `https://api.trello.com/1/cards/${id}`
  );
}

export async function deleteCheckList(id) {
  await axios.delete(
      `https://api.trello.com/1/checklists/${id}`
  );
}

export async function deleteTheCheckItem(id,itemid) {
  await axios.delete(
      `https://api.trello.com/1/checklists/${id}/checkItems/${itemid}`
  );
}


