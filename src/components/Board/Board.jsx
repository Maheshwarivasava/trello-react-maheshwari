import axios from "axios";
import { Box, Card, CardContent, CircularProgress, Typography } from "@mui/material";
import { ButtonBase } from '@mui/material';
import Errorpage from '../Errorpage.jsx';
import DisplayBoard from './DisplayBoard.jsx';
// import CreateBoard from './CreateBoard.jsx';
import CreatePopover from "../CreatePopover.jsx";
import { addBoarddata } from "../getdata.js";

const TOKEN = import.meta.env.VITE_API_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

const Board = ({ boards, setBoards, loading, setLoading, error, setError }) => {

    async function addBoard(boardName) {
        try {
            let res = await addBoarddata(boardName);
            setBoards((prevBoards) => [...prevBoards, res.data]);
        } catch (error) {
            setError("Cannot add board");
        }
    }

    return (
        <Box style={{ backgroundColor: '#FCFBFB', height: '100vh' }}>
            {error && <Errorpage errorType={error} />}
            {loading ? (
                <CircularProgress />
            ) : (
                <Box style={{ display: "flex", gap: "2rem", flexWrap: "wrap", marginTop: '2rem' ,marginLeft:'6rem'}} >
                    <ButtonBase >
                        <Card sx={{width: 300, height: 120,backgroundColor: "#E0EAEF",
                boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.1)", borderRadius: 2,
                transition: "transform 0.2s", "&:hover": { transform: "scale(1.05)" }}}>
                            <CardContent style={{ padding: '0' }}>
                                <CreatePopover onCreate={addBoard} itemName={"Board"}/>
                            </CardContent>
                        </Card>
                    </ButtonBase>
                    {boards && boards?.length === 0 ? (
                        <Typography variant="body1">No boards found.</Typography>
                    ) : (
                        boards.map((board, index) => (
                            <DisplayBoard key={index} board={board} index={index} />
                        ))
                    )}

                </Box>
            )}
        </Box>

    );
}

export default Board;
