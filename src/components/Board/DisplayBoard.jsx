import { ButtonBase, Card, CardContent, Typography } from "@mui/material";
import { Link } from "react-router-dom";

const DisplayBoard= ({ board, index }) =>{
    return (
        <ButtonBase>
            <Link to={`/boards/${board.id}`} style={{ textDecoration: 'none' }}>
                <Card key={index} sx={{
                    textAlign: "center", width: 300, height: 120,
                    background: board?.prefs?.backgroundImage
                        ? `url(${board?.prefs?.backgroundImage})`
                        : board?.prefs?.backgroundColor,
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.1)",
                    borderRadius: 2,
                    transition: "transform 0.2s",
                    "&:hover": {
                        transform: "scale(1.05)",
                    },
                }}
                >
                    <CardContent>
                        <Typography fontSize={16} fontWeight={600} >
                            {board.name}
                        </Typography>
                    </CardContent>
                </Card>
            </Link>
        </ButtonBase>
    );
}

export default DisplayBoard;