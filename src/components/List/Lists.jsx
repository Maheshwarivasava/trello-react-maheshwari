import { Box, Button, Card, CircularProgress, List, ListItem } from "@mui/material";
import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import DisplayList from "./DisplayList.jsx";
import CreateForm from "../CreateForm.jsx";
import Errorpage from "../Errorpage.jsx";
import { addListdata, deleteList, getdata } from "../getdata.js";

const Lists = () => {

    const [lists, setList] = useState([]);
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(true);

    const id = useParams().id;

    useEffect(() => {
        async function fetchData() {
            try {
                const data = await getdata("boards", "lists", id);
                setList(data);
                // console.log(data);
                setLoading(false);
            } catch (err) {
                setError(`There was an error retrieving the lists: ${err}`);
                setLoading(false);
            }
        }
        fetchData();
    }, []);

    const deleteListItem = (id) => {
        // console.log("hello", id);
        try {
            deleteList(id);
            setList((oldList) => oldList.filter((list) => list.id !== id));
        } catch (error) {
            setError(error.message);
        }
    }

    async function addList(listName) {
        try {
            // console.log(listName);
            let res = await addListdata(listName, id);
            // console.log('Hello');
            setList((lists) => [...lists, res.data]);
        } catch (error) {
            setError(`Cannot add List`);
        }
    }

    return (
        <Box>
            {error && <Errorpage errorType={error} />}
            {loading ? (
                <CircularProgress />
            ) : (
                <Box>
                    <Link to={"/boards"}>
                        <Button >Boards</Button>
                    </Link>
                    <List sx={{ height: '85vh', display: "flex", gap: "1rem", alignItems: 'flex-start', overflowX: 'scroll', "&:scrollbar": { display: "none" } }}>
                        {lists.map((list, index) => {
                            return (
                                <ListItem key={list.id} sx={{ width: "100vw", padding: '0rem', }}>
                                    <DisplayList
                                        list={list}
                                        index={index}
                                        deleteListItem={deleteListItem}
                                    />
                                </ListItem>
                            );
                        })}
                        <ListItem >
                            <Box sx={{ borderRadius: 2, minHeight: 150, backgroundColor: '#F2F2FC', width: '300px' }}>
                                <CreateForm addItem={addList} itemName={"List"} />
                            </Box>
                        </ListItem>
                    </List>
                </Box >
            )}
        </Box>
    )
}
export default Lists;