import React from "react";
import { Box, Select, Stack } from "@mui/material";
import { FormControl, MenuItem, InputLabel } from "@mui/material";
import { Link } from "react-router-dom";

function Navbars({ boards }) {
  return (
    <Stack direction={'row'} justifyContent={"space-between"} sx={{mt:1,bgcolor: "#D3E6F0" ,height:'5rem'}}>


      <Box style={{ display: "flex", height: "4rem",  justifyContent: 'space-between' }} >

        <Box
          sx={{
            ":hover": {
              backgroundImage: "url(https://trello.com/assets/87e1af770a49ce8e84e3.gif)",
            },
            backgroundImage: "url(https://trello.com/assets/d947df93bc055849898e.gif)",
            height: "100%",
            width: "6rem",
            ml: 4,
            mt:1,
            backgroundRepeat: "no-repeat",
            backgroundSize: "contain",
            backgroundPosition: "center",
            display: "block",
            filter: "brightness(0) saturate(100%) invert(30%) sepia(53%) saturate(323%) hue-rotate(179deg) brightness(91%) contrast(88%)",
          }}
        />
        <FormControl style={{ height:'3rem', width: '10rem', marginLeft: '4rem' ,marginTop:'0.8rem'}}>
          <InputLabel id="recent-boards-label" >Recent</InputLabel>
          <Select defaultValue="" label="Recent"  >
            <MenuItem value=""></MenuItem>
            {boards.map((board) => (
              
              <MenuItem key={board.id} value={board.id}>
                <Link to={`/boards/${board.id}`}>{board.name}</Link>
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>

    </Stack>
  );
}

export default Navbars;
