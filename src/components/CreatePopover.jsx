import React, { useState } from "react";
import { Button, Popover, TextField, Box, Typography, FormControl, FormLabel } from '@mui/material';

const CreatePopover = ({ onCreate, itemName }) => {
    const [newItemName, setNewItemName] = useState('');
    const [anchorEl, setAnchorEl] = useState(null);

    const handleInputChange = (e) => {
        setNewItemName(e.target.value);
    };

    const handlePopoverOpen = (e) => {
        setAnchorEl(e.currentTarget);
    };

    const handlePopoverClose = () => {
        setAnchorEl(null);
        setNewItemName('');
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        onCreate(newItemName);
        handlePopoverClose();
    };

    const open = Boolean(anchorEl);

    return (
        <div>
            <Button onClick={handlePopoverOpen} sx={{
                backgroundColor: "#E0EAEF", width: 300, height: 50,
                boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.1)", borderRadius: 2,
                transition: "transform 0.2s", "&:hover": { transform: "scale(1.05)" }
            }}>Create {`${itemName}`}</Button>
            <Popover
                open={open}
                anchorEl={anchorEl}
                onClose={handlePopoverClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Box display="flex" flexDirection="column" rowGap={3}
                    sx={{ alignItems: "center", borderRadius: 2, boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)", }}>
                    <Typography variant="h6" sx={{ color: "#0C0303", marginBottom: 1 }}>
                        Create
                    </Typography>
                    <form onSubmit={handleSubmit}>
                        <FormControl sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
                            <FormLabel sx={{ color: "#FCFBFB" }}>Title</FormLabel>
                            <TextField
                                type="text"
                                variant="outlined"
                                size="small"
                                sx={{ backgroundColor: "#F8F8FA", color: "#FCFBFB", }}
                                onChange={handleInputChange}
                            />
                            <Button
                                variant="contained"
                                type="submit"
                                sx={{ backgroundColor: "#81a1c1", color: "#ffffff", '&:hover': { backgroundColor: "#6397b8" } }}>
                                Create
                            </Button>
                        </FormControl>
                    </form>
                </Box>
            </Popover>
        </div>
    );
};

export default CreatePopover;
