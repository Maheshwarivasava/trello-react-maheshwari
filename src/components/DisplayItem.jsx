// import { Box, Button, Card, CardContent, Typography } from "@mui/material";
// import React from "react";
// import DeleteIcon from "@mui/icons-material/Delete";
// import Cards from "./Card/Cards.jsx";

// const DisplayItem = ({ item, index, deleteItem, itemName }) => {
//     return (
//         <Box>
//             <Card key={index} style={{ borderRadius: 5, minHeight: 150, backgroundColor: '#F2F2FC', width: '300px' }}>
//                 <Box style={{ display: "flex", justifyContent: "space-between" }}>
//                     <CardContent>
//                         <Typography fontSize={20} fontWeight={520}>
//                             {item.name}
//                         </Typography>
//                     </CardContent>
//                     <Button onClick={() => deleteItem(item.id)}>
//                         <DeleteIcon sx={{ color: "#94a0ac" }} />
//                     </Button>
//                 </Box>
//                 {itemName === "List" && <Cards id={item.id} />}
//             </Card>
//         </Box>
//     );
// }

// export default DisplayItem;
