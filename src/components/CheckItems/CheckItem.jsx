import { Box, CircularProgress, LinearProgress, List, ListItem, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import DisplayCheckItem from "./DisplayCheckItem";
import CreateForm from "../CreateForm";
import Errorpage from "../Errorpage";
import { addCheckItemdata, checkItemStatedata, deleteTheCheckItem, getdata } from "../getdata";

const CheckItem = ({ name, id, cardID }) => {
    const [checkitems, setCheckitems] = useState([]);
    const [error, setError] = useState(null);
    const [checked, setChecked] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchData() {
            try {
                const res = await getdata("checklists", "checkItems", id);
                setCheckitems(res);
                setChecked(res.state);
                setLoading(false);
            } catch (error) {
                setError("Cannot fetch checkitems");
                setLoading(false);
            }
        }
        fetchData();
    }, []);

    const deleteCheckItem = async (itemid) => {
        try {
            await deleteTheCheckItem(id, itemid);
            const updatedCheckitems = checkitems.filter((item) => item.id !== itemid);
            setCheckitems(updatedCheckitems);
        } catch (error) {
            setError(error.message);
        }
    };

    async function addCheckItem(name) {
        try {
            let res = await addCheckItemdata(name, id);
            setCheckitems((checkitems) => [...checkitems, res.data]);
        } catch (error) {
            console.log("Cannot add checkitem", error.message);
        }
    }
    async function checkItemState(checked, checkitemID) {
        try {
            let data = await checkItemStatedata(checked, checkitemID,cardID);
            data = data.data;
            // console.log(data);
            setChecked(data.state);
            setCheckitems((checkitems) =>
                checkitems.map((item) => {
                    if (item.id === checkitemID) {
                        item.state = checked;
                    }
                    return item;
                })
            );

        } catch (err) {
            console.log("Error occured: ", err.message);
        }
    };

    function progressTracker() {
        let checked = 0;
        checkitems.forEach((item) => {
            if (item.state === "complete") {
                checked++;
            }
        });
        let progressCalc = (checked / checkitems.length) * 100 || 0;
        // console.log(progressCalc);
        return parseInt(progressCalc);
    }


    return (
        <Box>
            {error && <Errorpage errorType={error} />}
            {loading ? (
                <CircularProgress />
            ) : (
                <List>
                    <Typography sx={{ fontSize: 15 }}>
                        {progressTracker()}%
                    </Typography>

                    <LinearProgress variant="determinate" color={progressTracker() === 100 ? "success" : "primary"} value={progressTracker()} sx={{ width: "100%" }} />
                    {checkitems.map((checkitem, index) => {
                        return (
                            <ListItem key={checkitem.id}>
                                <DisplayCheckItem
                                    checkitem={checkitem}
                                    index={checkitem.id}
                                    deleteCheckItem={deleteCheckItem}
                                    // setChecked={setChecked}
                                    cardID={cardID}
                                    checkItemState={checkItemState}
                                />
                            </ListItem>
                        );
                    })}
                    <ListItem>
                        <CreateForm addItem={addCheckItem} itemName={"CheckItem"} />
                    </ListItem>
                </List>
            )}
        </Box>
    );
};

export default CheckItem;
