import { Box, Button, Card, CardContent, Checkbox, FormControlLabel, Typography } from "@mui/material";
import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";


const DisplayCheckItem = ({ checkitem, index, deleteCheckItem, checkItemState}) => {

    return (
        <Box >
            <Card key={index} sx={{ borderRadius: 5, height: 60, backgroundColor: '#F2F2FC', width: '335px' ,marginTop:'0.5rem'}}>
                <CardContent sx={{ display: "flex", justifyContent: "space-between" ,alignItems:"center"}}>
                    <Box sx={{ display: "flex" }}>
                        <FormControlLabel
                            sx={{ margin: "0" }}
                            control={
                                <Checkbox
                                    checked={checkitem.state === "complete" }
                                    onChange={(e) => {
                                        // console.log(e.target.checked);
                                        if (e.target.checked) {
                                            checkItemState("complete",checkitem.id);
                                        } else {
                                            checkItemState("incomplete",checkitem.id);
                                        }
                                    }}
                                />
                            }
                        />
                        <Typography fontSize={16} fontWeight={400} style={{ marginTop: '0.2rem' }}>
                            {checkitem.name}
                        </Typography>
                    </Box>
                    <Button
                        onClick={() => {
                            deleteCheckItem(checkitem.id);
                        }}
                    >
                        <DeleteIcon sx={{ color: "#94a0ac" }} />
                    </Button>
                </CardContent>
               
            </Card>
        </Box>
    );
};

export default DisplayCheckItem;
