import { Box, Button, CircularProgress, List, ListItem } from "@mui/material";
import React, { useState, useEffect } from "react";
import DisplayCards from "./DisplayCards.jsx";
import CreateForm from "../CreateForm.jsx";
import Errorpage from "../Errorpage.jsx";
import { addCarddata, deleteCard, getdata } from "../getdata.js";


const Card = ({ id }) => {

    const [cards, setCards] = useState([]);
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(true);
    // console.log("Card id: ",id)
    useEffect(() => {
        try {
            async function fetchData() {
                const data = await getdata("lists", "cards", id);
                setCards(data);
                // console.log(data);
            }
            fetchData();
            setLoading(false);
        }
        catch (err) {
            setError(`There was an error retrieving the lists: ${err}`);
            setLoading(false);
        }
    }, []);

    const deleteCardItem = (id) => {
        // console.log("hello", id);
        try {
            deleteCard(id);
            setCards((oldCard) => oldCard.filter((card) => card.id !== id));
        } catch (error) {
            setError(error.message);
        }
    }

    async function addCard(CardName) {
        try {
            // console.log(CardName);
            let res = await addCarddata(CardName,id)
            // console.log('Hello');
            setCards((cards) => [...cards, res.data]);
        } catch (error) {
            setError(`Cannot add Card`);
        }
    }

    return (
        <Box>
            {error && <Errorpage errorType={error} />}
            {loading ? (
                <CircularProgress />
            ) : (
                <Box>
                    <Box sx={{ display: "flex", flexDirection: "column", gap: 2, paddingBottom: 2 }} >
                        <List sx={{ marginLeft: '0.8rem' }}>
                            {cards.map((card, index) => {
                                return (
                                    <DisplayCards
                                        key={card?.id}
                                        card={card}
                                        index={index}
                                        deleteCardItem={deleteCardItem}
                                    />
                                );
                            })}
                        </List>
                        <CreateForm addItem={addCard} itemName={"Card"} />
                    </Box>
                    {error && <Typography color="error">{error}</Typography>}
                </Box>
            )}
        </Box>
    )
}

export default Card;