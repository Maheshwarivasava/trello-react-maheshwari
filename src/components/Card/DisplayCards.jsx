import { Box, Button, Card, CardContent, Modal, Typography } from "@mui/material";
import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckList from "../CheckList/CheckList";

const DisplayCards = ({ card, index, deleteCardItem }) => {
    const [open, setOpen] = useState(false);
    const [error, setError] = useState(null);

    function handleOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <Box>
            <Card key={index} style={{ borderRadius: 5, width: 273, minHeight: 50 ,display: "flex", justifyContent: "space-between",marginTop:'1rem'}} onClick={handleOpen}>
                <CardContent>
                    <Typography fontSize={18} fontWeight={400}>
                        {card.name}
                    </Typography>
                </CardContent>
                <Button
                    onClick={() => {
                        deleteCardItem(card.id);
                    }} >
                    <DeleteIcon sx={{ color: "#94a0ac" }} />
                </Button>
            </Card>
            <Modal open={open} onClose={handleClose} sx={{ overflow: "scroll",    display: "flex", alignItems: "center", justifyContent: "center"}}>
                <CheckList name={card.name} id={card.id} />
            </Modal>
            {error && <Typography color="error">{error}</Typography>}
        </Box>
    )
}

export default DisplayCards;
