import { Box, Button, Card, CardContent, Typography } from "@mui/material";
import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckItem from "../CheckItems/CheckItem";

const DisplayCheckList = ({ checklist, index, deleteCheckListItem , cardID}) => {
    return (
        <Box>
            <Card key={index} sx={{ borderRadius: 2, minHeight: 100, backgroundColor: '#D8D8EE', width: '370px' }}>

                <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                    <CardContent >
                        <Typography fontSize={20} fontWeight={700} >
                            {checklist.name}
                        </Typography>
                    </CardContent>

                    <Button
                        onClick={() => {
                            deleteCheckListItem(checklist.id);
                        }}>
                        <DeleteIcon sx={{ color: "#94a0ac" }} />
                    </Button>
                </Box>

                <CheckItem name={checklist.name} id={checklist.id} cardID={cardID} />
            </Card>
        </Box>
    )
}

export default DisplayCheckList;
