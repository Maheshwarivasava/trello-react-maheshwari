import { Box, CircularProgress, List, ListItem, Typography } from "@mui/material";
import React, { useState, useEffect } from "react";
import DisplayCheckList from "./DisplayCheckList";
import CreatePopover from "../CreatePopover";
import Errorpage from "../Errorpage";
import { addCheckListdata, deleteCheckList, getdata } from "../getdata";

const CheckList = ({ name, id }) => {
    const [checklists, setChecklists] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchData() {
            try {
                const data = await getdata("cards", "checklists", id);
                setChecklists(data);
                setLoading(false);
            } catch (error) {
                setError("Cannot fetch checklists");
                setLoading(false);
            }
        }
        fetchData();
    }, []);

    async function addCheckList(name) {
        try {
            const res = await addCheckListdata(name,id)
            setChecklists((checklists) => [...checklists, res.data]);
        } catch (error) {
            setError("Cannot add checklist");
        }
    }

    const deleteCheckListItem = async (id) => {
        try {
            await deleteCheckList(id);
            setChecklists((oldChecklists) => oldChecklists.filter((checklist) => checklist.id !== id));
        } catch (error) {
            setError(error.message);
        }
    };

    return (
        <Box>
            {error && <Errorpage errorType={error} />}
            {loading ? (
                <CircularProgress />
            ) : (
                <Box sx={{ display: "flex", flexDirection: "column", gap: 2, paddingBottom: 2 }}>
                    <List sx={{ width: '400px', minHeight: 200, borderRadius: 2, backgroundColor: '#F2F2FC' }}>
                        {checklists.map((checklist, index) => (
                            <ListItem key={checklist.id}  >

                                <DisplayCheckList
                                    checklist={checklist}
                                    index={index}
                                    deleteCheckListItem={deleteCheckListItem}
                                    cardID={id}
                                />

                            </ListItem>
                        ))}
                        <ListItem sx={{ marginLeft: '2rem' }}>
                            <CreatePopover onCreate={addCheckList} itemName={"CheckList"} />
                        </ListItem>
                    </List>
                    {error && <Typography color="error">{error}</Typography>}
                </Box>
            )}
        </Box>
    );
};

export default CheckList;
